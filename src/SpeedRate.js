
let speedRate = 1;


function set(value) {
    speedRate = value;
}

function get() {
    return speedRate;
}
// class SpeedRate {

//     set(value) {
//         speedRate = value;
//     }

//     get() {
//         return speedRate;
//     }

// };

export const setSpeedRate = set;
export const getSpeedRate = get;
// export default SpeedRate;