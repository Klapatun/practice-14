import Card from './Card.js' 
import Game from './Game.js'
// import SpeedRate from './SpeedRate.js'

import {setSpeedRate as setGameSpeedRate} from './SpeedRate.js';


// Отвечает является ли карта уткой.
function isDuck(card) {
    return card && card.quacks && card.swims;
}

// Отвечает является ли карта собакой.
function isDog(card) {
    return card instanceof Dog;
}

// Дает описание существа по схожести с утками и собаками
function getCreatureDescription(card) {
    if (isDuck(card) && isDog(card)) {
        return 'Утка-Собака';
    }
    if (isDuck(card)) {
        return 'Утка';
    }
    if (isDog(card)) {
        return 'Собака';
    }
    return 'Существо';
}


//Основа для существа
class Creature extends Card {
    constructor(name, strength=0) {
        super(name, strength);
    }

    getDescriptions() {
        return [
            getCreatureDescription(this),
            super.getDescriptions(this),
        ];
    }
};

// Основа для утки.
class Duck extends Creature{
    constructor(name='Мирный житель', strength=2) {
        super(name, strength);
    }

    quacks() {
        console.log('quack')
    }

    swims() {
        console.log('float: both;')
    }
};


// Основа для собаки.
class Dog extends Creature {
    constructor(name='Бандит', strength=3) {
        super(name, strength);
    }

    swims() {
        console.log('float: none;')
    }
};

//Браток
class Lad extends Dog {
    constructor(name='Браток', strength=2) {
        super(name, strength);
    }

    static getInGameCount() {
        return this.inGameCount || 0;
    }

    static setInGameCount(value) { 
        // console.log(value);
        this.inGameCount = value; 
    }

    static getBonus() {
        let bonus = {};
        if(this.inGameCount > 1) {
            this.defenseBonus = this.inGameCount * ((this.inGameCount+1)/2);
            this.attackBonus = this.inGameCount * ((this.inGameCount+1)/2);
            bonus = {defenseBonus: this.defenseBonus, attackBonus: this.attackBonus};
        }

        return bonus;
    }

    doAfterComingIntoPlay(gameContext, continuation) {
        const {currentPlayer, oppositePlayer, position, updateView} = gameContext;
        continuation();

        Lad.setInGameCount(Lad.getInGameCount()+1);

    }

    doBeforeRemoving(continuation) {
        continuation();

        Lad.setInGameCount(Lad.getInGameCount()-1);
    }

    modifyDealedDamageToCreature (value, toCard, gameContext, continuation) {
        const bonus = Lad.getBonus();
        value = bonus.attackBonus || value;
        continuation(bonus.attackBonus);
    }

    modifyTakenDamage(value, fromCard, gameContext, continuation) {
        const bonus = Lad.getBonus();
        let damage = value;
        if(bonus.defenseBonus) {
            damage += value - bonus.defenseBonus;
        }
        continuation(damage);
    }


    getDescriptions() {
        const [nameCreature, pathObj] = super.getDescriptions(this);
        return [
            nameCreature,
            pathObj,
            `${(Lad.prototype.hasOwnProperty('modifyDealedDamageToCreature'))?'Чем их больше, тем они сильнее':''}`,
        ];
    }
}


//Основа для Изгоя
class Rogue extends Creature {
    constructor(name='Изгой', strength=2) {
        super(name,strength);
    }

};


// Колода Шерифа, нижнего игрока.
const seriffStartDeck = [
    new Duck('Слабый мирный житель', 1),
    new Duck(),
    new Duck(),
    new Rogue(),
    // new Card(),
    // new Card('Мирный житель', 2),
    // new Card('Мирный житель', 2),
];

// Колода Бандита, верхнего игрока.
const banditStartDeck = [
    // new Dog('Бандит', 3),
    // new Dog('Бандит', 4),
    new Lad(),
    new Lad(),
    new Lad(),
    // new Lad('Браток'),
];



// Создание игры.
const game = new Game(seriffStartDeck, banditStartDeck);

// Глобальный объект, позволяющий управлять скоростью всех анимаций.
setGameSpeedRate(1);

// Запуск игры.
game.play(false, (winner) => {
    alert('Победил ' + winner.name);
});
